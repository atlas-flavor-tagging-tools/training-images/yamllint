# yamllint

This project is a mirror of [https://github.com/sdesbure/docker_yamllint](https://github.com/sdesbure/docker_yamllint).
Due to changes in the [docker hub pull policy](https://www.docker.com/increase-rate-limits) it became necessary to host the image within the CERN gitlab container registry.

## README 

YAML lint - https://yamllint.readthedocs.io/en/latest/

Latest yamllint release in pip embedded into this docker.

Used to perform yaml linting into gitlab ci runner

